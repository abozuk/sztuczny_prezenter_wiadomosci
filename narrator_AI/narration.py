import os

from narrator_AI import ESpeak
import narrator_AI


class Narration:
    """
    Create narration

    Args:
        data_info - instance of DataCollector class

    Methods:
        text2speech - generate voice narration
    """

    def __init__(self, data_info):
        self._data = data_info

    def text2speech(self, output_path, gender="f"):
        """
        Generate voice narration modulated by
        the sentiment of the text.

        Args:
            output_path - audio output path
            gender - gender of narrator
                "f" - for female
                "m" - for male
        """
        # voice, narrator frames, speed
        narration_types = {
            (True, "f"): ["karen-happy", "Margaret_Thatcher", 72],
            (False, "f"): ["karen-sad", "angry_karen", 65],
            (True, "m"): ["bob-happy", "overcaffinated_jerry", 75],
            (False, "m"): ["bob-sad", "bald_kamil", 65],
        }

        text_path = self._data.text_path
        compound = self._data.sentiment

        narration_prop = narration_types[compound > 0, gender]
        narration = ESpeak(narration_prop[0])
        audio_len = narration.generate_text_to_speech(text_path, output_path)

        self._data.audio_fname = output_path
        self._data.audio_length = audio_len
        self._data.frames_path = os.path.join(narrator_AI.REPO_PATH, "narrator_frames", narration_prop[1])
        self._data.speed = narration_prop[2]

        # change this in dev process
        csv_output = output_path.replace(".wav", ".csv")
        self._data.save_csv(csv_output)
