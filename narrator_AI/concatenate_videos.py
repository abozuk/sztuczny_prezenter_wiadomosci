import os
import pandas as pd
from moviepy.editor import VideoFileClip, concatenate_videoclips


def list_to_cocncat(datafile, introfile, breakfile):

    """
    Function prepares list with names of files to concatenate.
    :param datafile: name of csv file with necessary metadata
    :param introfile: name/path of mp4 file with intro video
    :param breakfile: name/path of mp4 file to insert between news
    :return: list with names of files to concatenate
    """
    #prepare txt file with a list of videos to concatenate
    data_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "narrator_AI", datafile)

    df = pd.read_csv(data_file, header=[0])
    col = df["audio_anim"].str.strip()

    dfAsString = col.to_string(header=False, index=False)
    dfAsString = dfAsString.replace(" ", "")
    str_list = dfAsString.split('\n')
    vid_list = []

    for i in range(len(str_list)):
        if i == 0:
            vid_list.append(introfile)
        vid_list.append(str_list[i])
        if i != len(str_list)-1:
            vid_list.append(breakfile)

    return vid_list

def concatenate_vid(vid_list, output_path="", method="compose"):
    """
    Function combines mp4 files that are parts of a news video to one file.
    :param vid_list: list with names of files to concatenate
    :param output_path: path to output video with news
    :param method: method to concatenate videos, program works faster with compose method - recommended

    """
    clips = [VideoFileClip(c) for c in vid_list]
    if method == "reduce":
        # calculate minimum width & height across all clips
        min_height = min([c.h for c in clips])
        min_width = min([c.w for c in clips])
        # resize the videos to the minimum
        clips = [c.resize(newsize=(min_width, min_height)) for c in clips]
        # concatenate the final video
        final_clip = concatenate_videoclips(clips)
    elif method == "compose":
        # concatenate the final video with the compose method provided by moviepy
        final_clip = concatenate_videoclips(clips, method="compose")
    # write the output video file
    final_clip.write_videofile(output_path)


