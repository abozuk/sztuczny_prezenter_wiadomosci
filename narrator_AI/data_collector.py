import pandas as pd
import copy
import os

from narrator_AI import sentiment_analysis


class DataCollector:
    """
    Create metadata collector to combine the components
    of an animation into a complete unit

    Args:
        text_path - path to news text

    Methods:
        save_csv(output_path)
            output_path - path to csv file
    """
    def __init__(self, text_path):
        self._text_path = text_path
        self._common_words, self._compound = sentiment_analysis(text_path)
        self._common_words = [w[0] for w in self._common_words]
        self._df_dict = {
            "link": [None],
            "text_path": [self._text_path],
            "audio_fname": [None],
            "audio_length": [None],
            "speed": [None],
            "sentiment": [self._compound],
            "keywords": [str(self._common_words)],
            "frames_path": [None],
            "animation_fname": [None],
            "audio_anim": [None]
        }
        self._record_ctr = 0

    def save_csv(self, output_path):
        df = pd.DataFrame.from_dict(self._df_dict)
        if os.path.exists(output_path):
            self._record_ctr += 1
        df.to_csv(output_path, index=False)

    @property
    def link(self):
        return copy.copy(self._df_dict["link"][self._record_ctr])

    @link.setter
    def link(self, fname):
        self._df_dict["audio_fname"][self._record_ctr] = fname

    @property
    def text_path(self):
        return copy.copy(self._df_dict["text_path"][self._record_ctr])

    @property
    def audio_fname(self):
        return copy.copy(self._df_dict["audio_fname"][self._record_ctr])

    @audio_fname.setter
    def audio_fname(self, fname):
        self._df_dict["audio_fname"][self._record_ctr] = fname

    @property
    def audio_length(self):
        return copy.copy(self._df_dict["audio_length"][self._record_ctr])

    @audio_length.setter
    def audio_length(self, audio_len):
        self._df_dict["audio_length"][self._record_ctr] = audio_len

    @property
    def speed(self):
        return copy.copy(self._df_dict["speed"][self._record_ctr])

    @speed.setter
    def speed(self, s):
        self._df_dict["speed"][self._record_ctr] = s

    @property
    def sentiment(self):
        return copy.copy(self._df_dict["sentiment"][self._record_ctr])

    @sentiment.setter
    def sentiment(self, s):
        self._df_dict["sentiment"][self._record_ctr] = s

    @property
    def keywords(self):
        return copy.copy(self._df_dict["keywords"][self._record_ctr])

    @property
    def frames_path(self):
        return copy.copy(self._df_dict["frames_path"][self._record_ctr])

    @frames_path.setter
    def frames_path(self, path):
        self._df_dict["frames_path"][self._record_ctr] = path

    @property
    def animation_fname(self):
        return copy.copy(self._df_dict["animation_fname"][self._record_ctr])

    @animation_fname.setter
    def animation_fname(self, fname):
        self._df_dict["animation_fname"][self._record_ctr] = fname

    @property
    def audio_anim(self):
        return copy.copy(self._df_dict["audio_anim"][self._record_ctr])

    @audio_anim.setter
    def audio_anim(self, fname):
        self._df_dict["audio_anim"][self._record_ctr] = fname
